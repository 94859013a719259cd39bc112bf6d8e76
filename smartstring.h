#include <stdlib.h>
#include <string.h>

typedef struct {
  size_t size;
  int save;
  char *data;
} string;

void cleanstr(string *s) { free(s->data); }

string merge(string a, string b) {
  string s = {
    .size = a.size + b.size,
    .data = malloc(s.size),
    .save = 0
  };
  memcpy(s.data, a.data, a.size);
  memcpy(s.data+a.size, b.data, b.size);
  if (!a.save) cleanstr(&a);
  if (!b.save) cleanstr(&b);
  return s;
}

string str(char *ptr) {
  return (string) {
    .size = strlen(ptr),
    .save = 0,
    .data = strdup(ptr)
  };
}

string save(string s) { s.save = 1; return s; }

void print(const char *msg, string s) {
  printf("%s: ", msg);
  fwrite(s.data, 1, s.size, stdout);
  putchar('\n');
  if (!s.save) cleanstr(&s);
}

#define string string __attribute__((cleanup(cleanstr)))
